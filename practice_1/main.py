def fibonacci_function(n):
    if n in {0, 1}:
        return n
    return fibonacci_function(n - 1) + fibonacci_function(n - 2)


if __name__ == '__main__':
    assert fibonacci_function(1) == 1
    assert fibonacci_function(10) == 55
    assert fibonacci_function(14) == 377
